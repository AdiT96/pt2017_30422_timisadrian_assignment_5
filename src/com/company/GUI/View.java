package com.company.GUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Adi on 30-May-17.
 */
public class View {

    private JFrame frame;

    private JButton first = new JButton("Distinct Days");
    private JButton second = new JButton("Occurences of Actions");
    private JButton third = new JButton("Occurences each Day");
    private JButton fourth = new JButton("Activities longer than 10 Hours");
    private JButton fifth = new JButton("Activities 90% less than 5 Minutes");

    private JTextField text = new JTextField(20);

    private JPanel panel;

    public View(){

        frame = new JFrame("Java 8");

        frame.setSize(1080, 720);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        panel = new JPanel();

        JPanel p1 = new JPanel();
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        JPanel p4 = new JPanel();
        JPanel p5 = new JPanel();

        p1.add(first);
        p1.add(text);

        p2.add(second);
        p3.add(third);
        p4.add(fourth);
        p5.add(fifth);

        panel.add(p1);
        panel.add(p2);
        panel.add(p3);
        panel.add(p4);
        panel.add(p5);

        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        frame.setContentPane(panel);
        frame.setVisible(true);

    }

    public void addFirstListener(ActionListener fl){
        first.addActionListener(fl);
    }

    public void addSecondListener(ActionListener sl){
        second.addActionListener(sl);
    }

    public void addThirdListener(ActionListener tl){
        third.addActionListener(tl);
    }

    public void addFourthListener(ActionListener fol){
        fourth.addActionListener(fol);
    }

    public void addFifthListener(ActionListener fil){
        fifth.addActionListener(fil);
    }

    public void setText(String number){
        text.setText(number);
    }


}


