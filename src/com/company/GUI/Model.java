package com.company.GUI;

import com.company.Activities.ActivityManager;
import com.company.Activities.DateTime;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Map;

/**
 * Created by Adi on 30-May-17.
 */
public class Model {

    private ActivityManager activityManager;

    public Model() {

        this.activityManager = new ActivityManager();

    }

    public int getDistinctDays() {

        return activityManager.getDistinctDays();

    }

    public void writeSecondToFile() {

        Map<String, Integer> map = activityManager.determineNumberOfOccurences();

        try {
            PrintStream file = new PrintStream(new FileOutputStream("second.txt"));

            for (Map.Entry<String, Integer> entry : map.entrySet()) {

                file.println(entry.getKey() + "     " + entry.getValue());

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void writeThirdToFile() {

        try {
            PrintStream file = new PrintStream(new FileOutputStream("third.txt"));


            Map<Integer, Map<String, Integer>> map2 = activityManager.generateNumberOfOccurencesByDay();

            for (Map.Entry<Integer, Map<String, Integer>> entry : map2.entrySet()) {

                file.println("Day : " + entry.getKey());

                for (Map.Entry<String, Integer> entry2 : entry.getValue().entrySet()) {

                    file.println(entry2.getKey() + "    " + entry2.getValue());

                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void writeFourthToFile(){

        try {
            PrintStream file = new PrintStream(new FileOutputStream("fourth.txt"));

            Map<String, DateTime> map3 = activityManager.generateActivitiesLargerThanTenHours();

            for(Map.Entry<String, DateTime> entry : map3.entrySet()){

                file.println(entry.getKey() + "     " + entry.getValue());

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void writeFifthToFile(){

        try {
            PrintStream file = new PrintStream(new FileOutputStream("fifth.txt"));

            List<String> list = activityManager.activitiesLessThanFiveMinutes();

            for(String activity : list){

                file.println(activity);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }


}
