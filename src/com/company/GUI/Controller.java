package com.company.GUI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

/**
 * Created by Adi on 30-May-17.
 */
public class Controller {

    private View view;
    private Model model;

    public Controller(View view, Model model){

        this.view = view;
        this.model = model;

        view.addFirstListener(new FirstActionListener());
        view.addSecondListener(new SecondActionListener());
        view.addThirdListener(new ThirdActionListener());
        view.addFourthListener(new FourthActionListener());
        view.addFifthListener(new FifthActionListener());

    }

    public class FirstActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            view.setText(model.getDistinctDays() + "");
        }
    }

    public class SecondActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            model.writeSecondToFile();

            Desktop desk = Desktop.getDesktop();

            try {
                desk.open(new File("second.txt"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }


        }
    }

    public class ThirdActionListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            model.writeThirdToFile();

            Desktop desk = Desktop.getDesktop();

            try {
                desk.open(new File("third.txt"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }

    public class FourthActionListener implements ActionListener{


        @Override
        public void actionPerformed(ActionEvent e) {
            model.writeFourthToFile();

            Desktop desk = Desktop.getDesktop();

            try {
                desk.open(new File("fourth.txt"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public class FifthActionListener implements ActionListener{


        @Override
        public void actionPerformed(ActionEvent e) {

            model.writeFifthToFile();

            Desktop desk = Desktop.getDesktop();

            try {
                desk.open(new File("fifth.txt"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }

}
