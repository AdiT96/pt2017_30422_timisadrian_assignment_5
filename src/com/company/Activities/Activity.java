package com.company.Activities;

import java.time.LocalDateTime;

/**
 * Created by Adi on 29-May-17.
 */
public class Activity {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activityLabel;

    public Activity(LocalDateTime startTime, LocalDateTime endTime, String activityLabel) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityLabel = activityLabel;
    }

    public int getDay(){

        return startTime.getDayOfMonth();

    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public String toString(){

        return startTime + "   " + endTime + "   " + activityLabel;

    }

    public long getDuration(){

        if(endTime.getDayOfMonth() > startTime.getDayOfMonth()){

            long aux = (endTime.getSecond() + endTime.getMinute() * 60 + (endTime.getHour() + 24) * 3600) -
                    (startTime.getSecond() + startTime.getMinute() * 60 + startTime.getHour() * 3600);

            return aux;

        }else{

            long aux = (endTime.getSecond() + endTime.getMinute() * 60 + endTime.getHour() * 3600) -
                    (startTime.getSecond() + startTime.getMinute() * 60 + startTime.getHour() * 3600);

            return aux;

        }

    }
}
