package com.company.Activities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Adi on 29-May-17.
 */
public class ActivityManager {

    private List<Activity> activities;

    public ActivityManager() {

        activities = new LinkedList<>();

        try {

            FileReader fr = new FileReader("Activities.txt");
            BufferedReader file = new BufferedReader(fr);

            String line = file.readLine();
            do {

                String[] labels = line.split("\\s+");

                String[] startdDates = labels[0].split("-");
                String[] startTimes = labels[1].split(":");
                String[] endDates = labels[2].split("-");
                String[] endTimes = labels[3].split(":");

                activities.add(new Activity(LocalDateTime.of(Integer.parseInt(startdDates[0]), Integer.parseInt(startdDates[1]), Integer.parseInt(startdDates[2]),
                        Integer.parseInt(startTimes[0]), Integer.parseInt(startTimes[1]), Integer.parseInt(startTimes[2])),
                        LocalDateTime.of(Integer.parseInt(endDates[0]), Integer.parseInt(endDates[1]), Integer.parseInt(endDates[2]),
                                Integer.parseInt(endTimes[0]), Integer.parseInt(endTimes[1]), Integer.parseInt(endTimes[2])),
                        labels[4]));


                line = file.readLine();

            } while (line != null);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public int getDistinctDays() {

        return activities
                .stream()
                .map(Activity::getDay)
                .distinct()
                .collect(Collectors
                        .toList()).size();

    }

    public Map<String, Integer> determineNumberOfOccurences() {

        Map<String, Integer> map = activities
                .stream()
                .collect(Collectors
                        .groupingBy(Activity::getActivityLabel, Collectors
                                .summingInt(p -> 1)));


        return map;
    }

    public Map<Integer, Map<String, Integer>> generateNumberOfOccurencesByDay() {

        Map<Integer, Map<String, Integer>> map = activities
                .stream()
                .collect(Collectors
                        .groupingBy(Activity::getDay, Collectors
                                .groupingBy(Activity::getActivityLabel, Collectors
                                        .summingInt(p -> 1))));

        return map;

    }

    public List<String> activitiesLessThanFiveMinutes() {

        Map<String, Integer> map = activities.stream()
                .filter(p -> p.getDuration() <= 5 * 60)
                .collect(Collectors
                        .groupingBy(Activity::getActivityLabel, Collectors
                                .summingInt(p -> 1)));

        Map<String, Integer> map2 = determineNumberOfOccurences();

        List<String> list = map
                .entrySet()
                .stream()
                .filter(p -> map.get(p.getKey()) * 1.0 / map2.get(p.getKey()) >= 0.9)
                .map(p -> p.getKey())
                .collect(Collectors.toList());

        return list;

    }

    public Map<String, DateTime> generateActivitiesLargerThanTenHours() {

        Map<String, DateTime> map = activities
                .stream()
                .collect(Collectors
                        .groupingBy(Activity::getActivityLabel, Collectors
                                .summingLong(Activity::getDuration)))
                .entrySet()
                .stream()
                .filter(p -> p.getValue() >= 36000)
                .collect(Collectors
                        .toMap(p -> p.getKey(), p -> new DateTime(p.getValue())));

        return map;

    }


}
