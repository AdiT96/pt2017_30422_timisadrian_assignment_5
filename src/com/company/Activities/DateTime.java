package com.company.Activities;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by Adi on 29-May-17.
 */
public class DateTime {

    private Duration duration;

    public DateTime() {

        duration = Duration.ofSeconds(0);

    }

    public DateTime(long seconds){

        duration = Duration.ofSeconds(seconds);

    }

    public void add(long time) {

        duration.plusSeconds(time);

    }


    public String toString() {

        return duration + "";
    }

}
