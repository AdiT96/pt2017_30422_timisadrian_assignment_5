package com.company;

import com.company.GUI.Controller;
import com.company.GUI.Model;
import com.company.GUI.View;

public class Main {

    public static void main(String[] args) {

        View view = new View();
        Model model  = new Model();
        Controller controller = new Controller(view, model);

    }
}
